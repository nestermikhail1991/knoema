document.addEventListener('DOMContentLoaded', () => {
	console.log('loaded')
	const articles = document.querySelectorAll('.article');
	articles.forEach((article, index) => {
		article.addEventListener('click', () => {
			articles.forEach(article => article.classList.remove('active'))
			article.classList.add('active')
		});
		article.style.transform = `translateX(${-50*index}px)`
	});

	const searchForm = document.querySelector('.search-container');
	searchForm.addEventListener('submit', (e) => {
		e.preventDefault();
		const text = document.querySelector('.search-text').value;
		window.open(`https://www.google.com/search?q=${text}`);
	});
});
